import grails.util.Environment


Environment.executeForEnvironment(Environment.DEVELOPMENT, {
    grails.server.port.http = 8082
})

grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.7
grails.project.source.level = 1.7
grails.project.war.file = "target/${appName}.war"
grails.server.port.http = 8082
//grails.project.war.file = "target/${appName}-${appVersion}.war"
dependencyManager.ivySettings.defaultCacheIvyPattern = "[organisation]/[module](/[branch])/ivy-[revision](-[classifier]).xml"

// Remove the JDBC jar before the war is bundled
// Test

grails.project.fork = [
        test   : false,
        run    : false,
        war    : false,
        console: false
]//disabled

grails.project.dependency.resolver = "ivy" // or ivy

dependencyManager.ivySettings.defaultCacheIvyPattern = "[organisation]/[module](/[branch])/ivy-[revision](-[classifier]).xml"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve true
    repositories {
        inherits true

        grailsPlugins()
        grailsHome()
        grailsCentral()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        mavenLocal()
        mavenCentral()
//    grailsRepo 'http://repo.grails.org'
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://repo.grails.org/grails/libs-releases-local"
        mavenRepo("http://errigalArtifactory:8080/artifactory/libs-releases-local")
        mavenRepo("http://errigalArtifactory:8080/artifactory/plugins-releases-local")
        mavenRepo("http://errigalArtifactory:8080/artifactory/errigal-plugins")
        mavenRepo("http://errigalArtifactory:8080/artifactory/errigal-libs")
        mavenRepo "https://oss.sonatype.org/content/repositories/snapshots"
        mavenRepo 'http://repo.grails.org/grails/plugins'
        mavenRepo 'http://repo.grails.org/grails/plugins-releases'
        mavenRepo "http://repo.grails.org/grails/repo/"
        mavenRepo "https://repo1.maven.org/maven2"
        mavenRepo "http://repository.springsource.com/maven/bundles/release"
        mavenRepo "http://repository.springsource.com/maven/bundles/external"

    }


    dependencies {
        compile 'com.rabbitmq:amqp-client:3.6.5'

        runtime "hsqldb:hsqldb:1.8.0.10"
        runtime 'com.github.groovy-wslite:groovy-wslite:1.1.2'

        //required due to an error with the GWT plugin not compiling, haven't got time to investigate properly - john
        compile "org.tmatesoft.svnkit:svnkit:1.3.3", {
            excludes "jna", "trilead-ssh2", "sqljet"
        }

        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        runtime 'mysql:mysql-connector-java:5.1.25'/*,'javax.validation:validation-api:1.0.0.GA'*/
        test 'mysql:mysql-connector-java:5.1.25'

        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
          compile("org.apache.shiro:shiro-quartz:1.2.2") {
        http://stackoverflow.com/questions/4224484/why-do-grails-quartz-jobs-die-after-a-few-minutes-on-production
           excludes("quartz")
          }

        compile("org.apache.httpcomponents:httpclient:4.0.3") {
            //http://stackoverflow.com/questions/4224484/why-do-grails-quartz-jobs-die-after-a-few-minutes-on-production
            excludes("commons-codec")
        }
        compile "javax.validation:validation-api:1.0.0.GA"



        runtime('org.codehaus.groovy.modules.http-builder:http-builder:0.5.0') {
            excludes 'xalan', 'xml-apis', 'groovy', 'httpclient', 'commons-codec'
        }
        compile('org.jsoup:jsoup:1.8.3') {
            excludes 'junit'
        }

        compile 'com.google.code.gson:gson:2.5'
//required by errigal-user-api but excluded it to make sure the validation api is removed
        compile 'com.google.guava:guava:12.0.1'
        //THESE ARE REQUIRED BECAUSE WE ARE USING IVY!  MUST BE LOOKED AT EVERY GRAILS UPGRADE!!!!!
        runtime 'org.springframework:spring-aop:4.1.9.RELEASE'//grails needs this and can't resolve it with ivy, be sure to match the other spring jar versions
        runtime 'org.springframework:spring-expression:4.1.9.RELEASE'//grails needs this and can't resolve it with ivy, be sure to match the other spring jar versions
        test 'org.hamcrest:hamcrest-all:1.3'//spock needs this and can't resolve it
        compile 'cglib:cglib:2.2.2'//grails needs this and apate can't find it
        compile 'commons-io:commons-io:2.1'//the GWT plugin needs this
        compile 'org.codehaus.groovy:groovy-all:2.4.5'//required because of this issue https://github.com/grails/grails-core/issues/10011#issuecomment-245138488
//required by errigal-user-api but excluded it to make sure the validation api is removed

        compile group: 'org.apache.commons', name: 'commons-lang3', version: '3.4'
        compile group: 'org.apache.commons', name: 'commons-compress', version: '1.13' // IDMS-1581 - Required For The TEKO TELECOM Remote Components Retrieval

        compile group: 'org.codehaus.gpars', name: 'gpars', version: '1.2.1'
        compile 'org.codehaus.jsr166-mirror:jsr166y:1.7.0'

        test group: 'com.github.tomakehurst', name: 'wiremock', version: '2.5.1'

        // https://mvnrepository.com/artifact/org.codehaus.groovy.modules.http-builder/http-builder
        compile group: 'org.codehaus.groovy.modules.http-builder', name: 'http-builder', version: '0.7.1'
    }

    plugins {
        compile ':build-test-data:2.4.0'//doesn't work when test?
        compile ":jdbc-pool:7.0.47"
        compile ":executor:0.2"
        compile ":yui:2.8.2"
        compile ":bubbling:2.1.4"
        compile ":mail:1.0.7"
        compile ':quartz:1.0.2'

        compile ":remoting:1.3", {
            excludes 'hessian'
        }
        compile ':shiro:1.2.1'

        compile ":scaffolding:2.1.2"
        compile ':cache:1.1.8'
        // asset-pipeline 2.0+ requires Java 7, use version 1.9.x with Java 6
        compile ":asset-pipeline:2.5.7"

        // plugins needed at runtime but not for compilation
        runtime ":hibernate4:4.3.10" // or ":hibernate:3.6.10.18"
        runtime ":database-migration:1.4.0"

        compile ":extended-dependency-manager:0.5.5"

        compile ':tomcat:7.0.70'

        runtime ":jquery:1.11.1"
        runtime ':resources:1.2.14'

        compile(':gwt:0.9.1')

        //errigal reporting dependencies start
        compile ":mail:0.9"

    }
}

def intendedGWTVersion = '2.5.1'
gwt.version = "2.5.1"

gwt {
    output.style = 'DETAILED'
    run.args = {
        jvmarg(value: '-Xmx1024m')
    }
}