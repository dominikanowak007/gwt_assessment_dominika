package dn_gwt

import dn_gwt.client.SearchAction
import dn_gwt.client.SearchResponse

class SearchActionHandler {

    SearchResponse execute(SearchAction action) {

       ServerDataService clockService = new ServerDataService()
       String currentServerTime = clockService.getCurrentServerTime()

        return new SearchResponse(currentServerTime)
    }
}
