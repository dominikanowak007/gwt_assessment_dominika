package dn_gwt;

import java.io.Serializable;

public class ServerClock implements Serializable {

  private  String serverTime;

    public ServerClock(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }
}
