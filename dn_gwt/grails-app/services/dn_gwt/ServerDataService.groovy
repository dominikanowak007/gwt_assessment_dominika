package dn_gwt

import grails.transaction.Transactional

import java.text.DateFormat
import java.text.SimpleDateFormat

@Transactional
class ServerDataService {

    static transaction=true

    static expose = [ 'gwt:dn_gwt.client']
    ServerDataService () {}

    String getCurrentServerTime()
    {
        Date currentTime = new Date()
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy  HH:mm:ss");
        ServerClock clock = new ServerClock( dateFormat.format(currentTime))

        return clock.getServerTime()
    }

}
