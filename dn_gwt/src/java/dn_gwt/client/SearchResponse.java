package dn_gwt.client;

import grails.plugins.gwt.shared.Response;


public class SearchResponse implements Response {

    private static final long serialVersionUID = 1L;

    public String timeString;

    public SearchResponse() {}

    public SearchResponse(String timeString)
    {
        this.timeString=timeString;
    }
}


